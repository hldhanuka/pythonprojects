import re

line = "Learn Python through through tutorials on javatpoint"

match_object = re.match( r'through', line, re.M|re.I)

if match_object:
    print("match object group : ", match_object.group())
else:
    print( "There isn't any match!!")

search_object = re.search( r'through', line, re.M|re.I)

if search_object:
    print("search object group : ", search_object.group(0))
else:
    print("Nothing found!!")

search_object = re.search( r'through (through)', line, re.M|re.I)

if search_object:
    print("search object group : ", search_object.group(0))
    print("search object group : ", search_object.group(1))
else:
    print("Nothing found!!")