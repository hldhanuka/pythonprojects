# Creating Objects (instance) in Python

class Employee:
    id = 10
    name = "Devansh"

    def display (self):
        print(self.id, self.name)

employee = Employee()

employee.display()