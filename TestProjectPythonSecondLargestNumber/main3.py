# declaring empty list
list_val = []

# user provides the number of elements to be added in the list
num_list = int(input("Enter number of elements in list: "))

if num_list <= 1:
    print("Enter More Than 1 Value")
else :
    for i in range(1, num_list + 1):
        element = int(input("Enter the elements: "))

        list_val.append(element)

    # sort the list
    list_val.sort()

    # print second largest element
print("Second largest element is:", list_val[-2])