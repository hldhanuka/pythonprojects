# Traversing the list

def calc_largest(arr):
    second_largest = arr[0]
    largest_val = arr[0]

    for i in range(len(arr)):
        if arr[i] > largest_val:
            largest_val = arr[i]

    for i in range(len(arr)):
        if arr[i] > second_largest and arr[i] != largest_val:
            second_largest = arr[i]

    return second_largest

print(calc_largest([20, 30, 40, 25, 10]))