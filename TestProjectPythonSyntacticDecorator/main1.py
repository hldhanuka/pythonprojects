def outer_div(func):
    def inner(x, y):
        if(x < y):
            print(f"{x} - {y}")

            x, y = y, x

            print(f"{x} - {y}")

        return func(x, y)

    return inner

# syntax of generator
@outer_div
def divide(x, y):

    print("\n")

    print(x / y)

divide(2, 4) #### this is the bast way to do this, we think of above example - "Decorating functions with parameters"