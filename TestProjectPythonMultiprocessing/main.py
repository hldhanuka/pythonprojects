from multiprocessing import Process
import os

def disp():
    print('Hello !! Welcome to Python Tutorial')

if __name__ == '__main__':
    p = Process(target = disp)

    print("before start")

    p.start()

    print("before join")

    p.join()