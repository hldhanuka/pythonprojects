from mod_decorator import do_twice

@do_twice
def say_hello(name):
    print(f"Hello There {name}")

say_hello("Dhanuka")