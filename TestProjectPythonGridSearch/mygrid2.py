import sys
import pandas as pd
import numpy as np
from sklearn.model_selection import GridSearchCV, KFold
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.wrappers.scikit_learn import KerasClassifier
from keras.optimizers import Adam

#  importing  the  dataset
mydf = pd.read_csv("Diet_Dataset.csv")

#  printing  the  first  five  lines  of  dataset
print(mydf.head())

print('\n')

# converting dataframe into numpy array
mydataset = mydf.values

X = mydataset[:, 0:6]
Y = mydataset[:, 6].astype(int)

# Normalizing the data using sklearn StandardScaler
from sklearn.preprocessing import StandardScaler

myscaler = StandardScaler().fit(X)

# Transforming and displaying the training data
X_stdized = myscaler.transform(X)

mydata = pd.DataFrame(X_stdized)

def create_my_model(learnRate, dropoutRate):
    # Creating the model
    mymodel = Sequential()

    mymodel.add(Dense(6, input_dim = 6, kernel_initializer = 'normal', activation = 'relu'))
    mymodel.add(Dropout(dropoutRate))
    mymodel.add(Dense(3, input_dim = 6, kernel_initializer = 'normal', activation = 'relu'))
    mymodel.add(Dropout(dropoutRate))
    mymodel.add(Dense(1, activation = 'sigmoid'))

    # Compile the model
    myadam = Adam(learning_rate = learnRate)
    mymodel.compile(loss = 'binary_crossentropy', optimizer = myadam, metrics = ['accuracy'])

    return mymodel

# Creating the model object
mymodel = KerasClassifier(build_fn = create_my_model, verbose = 1)

# Defining the arguments that we want to use in Grid Search along
# with the list of values that we want to try out
learnRate = [0.001, 0.02, 0.2]
dropoutRate = [0.0, 0.2, 0.4]
batchSize = [10, 20, 30]
epochs = [1, 5, 10]

# Making a dictionary of the grid search parameters
paramgrid = dict(learnRate = learnRate, dropoutRate = dropoutRate, batch_size = batchSize, epochs = epochs )

# Building and fitting the GridSearchCV
mygrid = GridSearchCV(estimator = mymodel, param_grid = paramgrid, cv = KFold(random_state = None), verbose = 10)

gridresults = mygrid.fit(X_stdized, Y)

# an empty string
converted_best_params = str()

# iterating over dictionary using a for loop
for key in gridresults.best_params_:
    converted_best_params += key + ": " + str(gridresults.best_params_[key]) + ", "

# Summarizing the results in a readable format
print("Best: " + str(gridresults.best_score_) + " using " + converted_best_params)

means = gridresults.cv_results_['mean_test_score']
stds = gridresults.cv_results_['std_test_score']
params = gridresults.cv_results_['params']

for mean, stdev, param in zip(means, stds, params):
    print(mean + "(" + stdev + ")" + " with: " + param)