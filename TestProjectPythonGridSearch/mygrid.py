import sys
import pandas as pd
import numpy as np
from sklearn.model_selection import GridSearchCV, KFold
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.wrappers.scikit_learn import KerasClassifier
from keras.optimizers import Adam

#  importing  the  dataset
mydf = pd.read_csv("Diet_Dataset.csv")

#  printing  the  first  five  lines  of  dataset
print(mydf.head())

print('\n')

# converting dataframe into numpy array
mydataset = mydf.values

X = mydataset[:, 0:6]
Y = mydataset[:, 6].astype(int)

# Normalizing the data using sklearn StandardScaler
from sklearn.preprocessing import StandardScaler

myscaler = StandardScaler().fit(X)

# Transforming and displaying the training data
X_stdized = myscaler.transform(X)

mydata = pd.DataFrame(X_stdized)

# defining the function to create model
def create_my_model(learnRate, dropoutRate):
    # Creating model
    mymodel = Sequential()

    mymodel.add(Dense(6, input_dim = 6, kernel_initializer = 'normal', activation = 'relu'))
    mymodel.add(Dropout(dropoutRate))
    mymodel.add(Dense(3, input_dim = 6, kernel_initializer = 'normal', activation = 'relu'))
    mymodel.add(Dropout(dropoutRate))
    mymodel.add(Dense(1, activation = 'sigmoid'))

    # Compiling the model
    my_Adam = Adam(learning_rate = learnRate)

    mymodel.compile(loss = 'binary_crossentropy', optimizer = my_Adam, metrics = ['accuracy'])

    return mymodel

####### ------------------------ Training the Model without Grid Search

# Declaring the values of the parameter
dropoutRate = 0.1
epochs = 1
batchSize = 20
learnRate = 0.001

# Creating the model object by calling the create_my_model function we created above
mymodel = create_my_model(learnRate, dropoutRate)

# Fitting the model onto the training data
mymodel.fit(X_stdized, Y, batch_size = batchSize, epochs = epochs, verbose = 1)

####### ------------------------ Training the Model without Grid Search