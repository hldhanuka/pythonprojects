from EmptyError import EmptyError

var = " "

try:
    raise EmptyError("The variable is empty")
except EmptyError as e:
    print(e)
