class EmptyError(RuntimeError):
    def __init__(self, argument):
        self.arguments = argument
