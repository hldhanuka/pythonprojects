# Using default function as Python decorators
def Python_Decorator(funct):
       # Inner nested function
       def inner():
              print("This line of code will be printed before the execution of high order function")
              funct()
              print("This line of code will be printed after the execution of high order function")

       return inner

# A default function as decorator
def JTP_Decorator():
    print("This line of code will be printed inside the execution of high order function")

JTP_Decorator = Python_Decorator(JTP_Decorator) # Python decorator as high order function

# Python decorator calling out as high order function
JTP_Decorator()