# Update Operation

import mysql.connector

#Create the connection object
myconn = mysql.connector.connect(host = "localhost", user = "root", passwd = "", database = "python_test_db_connection")

#creating the cursor object
cur = myconn.cursor()

try:
    #updating the name of the employee whose id is 110
    cur.execute("update Employee set name = 'alex' where id = 110")

    myconn.commit()
except:
    myconn.rollback()

myconn.close() 