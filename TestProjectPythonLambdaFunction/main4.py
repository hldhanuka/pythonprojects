## Using Lambda Function with if-else

# Code to use lambda function with if-else
Minimum = lambda x, y : x if (x < y) else y

print(Minimum( 35, 74 ))