### Using Lambda Function with filter() / map() 

# Code to filter odd numbers from a given list
list_ = [34, 12, 64, 55, 75, 13, 63]

odd_list = list(filter(lambda num: (num % 2 != 0) , list_ ))

print(odd_list)

#### ----------------------------------------------------------------------

# Code to calculate the square of each number of a list using the map() function

numbers_list = [2, 4, 5, 1, 3, 7, 8, 9, 10]

squared_list = list(map(lambda num: num ** 2 , numbers_list))

print( squared_list )