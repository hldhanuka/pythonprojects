# Using Lambda Function with List Comprehension

#Code to calculate square of each number of list using list comprehension
squares = [lambda num = num: num ** 2 for num in range(0, 11)]

for square in squares:
    print( square(), end = " ")