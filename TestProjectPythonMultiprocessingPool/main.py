from multiprocessing import Pool
import time

w = (["V", 5], ["X", 2], ["Y", 1], ["Z", 3])

def work_log(data_for_work):
    print(" Process name is %s waiting time is %s seconds" % (data_for_work[0], data_for_work[1]))

    time.sleep(int(data_for_work[1]))

    print(" Process %s Executed." % data_for_work[0])


def handler():
    p = Pool(2)

    p.map(work_log, w)

if __name__ == '__main__':
    handler()