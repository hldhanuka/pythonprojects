def add(** vals: int) :
    t_val = 0

    for i in vals.items() :
        t_val += i[1]

    print(f'**{vals} : {t_val}')

add(a = 1)
add(a = 1, b = 2)
add(a = 1, b = 2, c = 3)