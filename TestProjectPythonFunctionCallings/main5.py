def add(* a: int) :
    t_val = 0

    for i in a :
        t_val += i

    print(f'*{a} : {t_val}')

add(1)
add(1, 2)
add(1, 2, 3)