from mod_decorator import do_twice

@do_twice
def say_hello(name):
    print(f"Hello There {name}")

    return f"Hi {name}"

hi_val = say_hello("Dhanuka")

print(hi_val)